#!/usr/bin/perl
use DBI;
my $dbh = DBI->connect("dbi:mysql:porridge:127.0.0.1:3306", "signup", "Amish53[wave") 
        or die "Can't connect";

my $sql = "SELECT id, old_coms_login AS login FROM customers";

my %ids;

my $sth = $dbh->prepare($sql);
$sth->execute();

while (my $row = $sth->fetchrow_hashref()) {
    my $id = $row->{'id'};
    my $login = $row->{'login'};
    
    $ids{$login} = $id;
}

$sth->finish();

$sql = "UPDATE dids SET customer = ? WHERE old_coms_customer = ?";

foreach my $login (keys %ids) {
    $sth = $dbh->prepare($sql);
    $sth->execute($ids{$login}, $login);
    $sth->finish();
}
