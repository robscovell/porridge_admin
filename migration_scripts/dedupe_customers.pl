#!/usr/bin/perl

use DBI;
use Data::Dumper;
use List::Util qw( min max );

my $dbh = DBI->connect("dbi:mysql:porridge:127.0.0.1:3306", "signup", "Amish53[wave") 
        or die "Can't connect";

#my $sql = "UPDATE dids SET customer = ? WHERE customer = ?";
my $sql = "DELETE FROM customers WHERE email = ? AND id <> ?";

my %emails;

while(<>) {
    my ($email, $id) = split(/,/);
    unless ($emails{$email}) {
        $emails{$email} = [];
    }
    push @{$emails{$email}}, $id;
}

# for my $email (keys %emails) {
#     my @ids = @{$emails{$email}};
#     my $min = min @ids;
#     my $sth = $dbh->prepare($sql);
#     for my $id (@ids) {
#         $sth->execute($min, $id);
#     }
#     $sth->finish();
# }

for my $email (keys %emails) {
    my @ids = @{$emails{$email}};
    my $min = min @ids;
    my $sth = $dbh->prepare($sql);
    $sth->execute($email, $min);
    $sth->finish();
}