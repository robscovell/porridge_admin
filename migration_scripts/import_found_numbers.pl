#!/usr/bin/perl
use DBI;
my $dbh = DBI->connect("dbi:mysql:porridge:127.0.0.1:3306", "signup", "Amish53[wave") 
        or die "Can't connect";

my $sql_find_did = "SELECT number FROM dids WHERE area = ? AND issued = 0 LIMIT 1";
my $sql_find_cst = "SELECT id FROM customers WHERE old_coms_login = ?";

my $sql_update = "UPDATE dids SET target = ?, issued = 1, customer = ?, old_coms_did = ?, old_coms_customer = ? WHERE number = ?";

while(<>) {
    my ($old_coms_customer, $old_coms_did, $target) = split(/,/);
    chomp $target;
    my ($area) = $old_coms_did =~ /(0....)/;
    $area =~ s/(01[234569]1).*/$1/;
    $area =~ s/(02.).*/$1/;
    $area =~ s/(011.).*/$1/;

    my $sth_find_did = $dbh->prepare($sql_find_did);
    $sth_find_did->execute($area);
    my $row = $sth_find_did->fetchrow_hashref();
    if ($row) {
        my $new_did = $row->{'number'};
        my $sth_find_cst = $dbh->prepare($sql_find_cst);
        $sth_find_cst->execute($old_coms_customer);
        my $row_cst = $sth_find_cst->fetchrow_hashref();
        my $id_cst = $row_cst->{'id'};
        $sth_find_cst->finish();
        print "$id_cst,$old_coms_customer,$old_coms_did,$new_did,$target\n";
        my $sth_update = $dbh->prepare($sql_update);
        $sth_update->execute($target, $id_cst, $old_coms_did, $old_coms_customer, $new_did);
        $sth_update->finish();
    }
    $sth_find_did->finish();
}