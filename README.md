This is the admin application for the prisoner telephone service I talked about in the interview. 

The project was successful until the prison service copied the idea and implemented their own 'official' version.

I own the IP rights to this code, so there is no issue with me sharing it with you.