from django.contrib import admin
from accounts.models import Customers, Dids, Transactions, Cdrs

from django.http import HttpResponse

import csv
from django.utils.encoding import smart_str

from django.db.models import Q

admin.site.disable_action('delete_selected')

class DDIInline(admin.TabularInline):
    model = Dids
    fields = ('number','target', )
    readonly_fields = ('number','target', )
    max_num=0
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False    

class CdrsInline(admin.TabularInline):
    model = Cdrs
    fields = ('connect', 'msisdn', 'cli', 'cld', 'duration', 'charge')
    readonly_fields =('connect', 'msisdn', 'cli', 'cld', 'duration', 'charge')
    max_num=0
    
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False    
    
    def charge(self, obj):
        amount = obj.duration
        if amount is None:
            return amount
        else:
            return int((amount + 59) / 60) * 10
        
class TransactionsInline(admin.TabularInline):
    model = Transactions
    fields = ('timestamp', 'transactionnumber', 'amount', 'completed')
    readonly_fields =  ('timestamp', 'transactionnumber', 'amount', 'completed')
    max_num=0
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False    



@admin.register(Customers)
class CustomerAdmin(admin.ModelAdmin):
    search_fields = ['email', 'lastname', 'login']
    inlines = [ DDIInline, TransactionsInline, CdrsInline]
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False    

def export_transaction_csv(model_admin, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=transactions.csv'
    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8')) # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        smart_str(u"login"),
        smart_str(u"Email"),
        smart_str(u"Name"),
        smart_str(u"Date"),
        smart_str(u"transactionnumber"),
        smart_str(u"Amount"),
        smart_str(u"tc"),
    ])
    for obj in queryset:
        writer.writerow([
            smart_str(obj.customer.login),
            smart_str(obj.customer.email),
            smart_str("%s, %s" % (obj.customer.lastname, obj.customer.firstname)),
            smart_str(obj.timestamp),
            smart_str(obj.transactionnumber),
            smart_str(obj.amount),
            smart_str(obj.tc),
        ])
    return response

export_transaction_csv.short_description = u"Export CSV"


@admin.register(Transactions)
class TransactionAdmin(admin.ModelAdmin):
    search_fields = ['transactionnumber', 'timestamp']
    fields = ('email', 'timestamp', 'transactionnumber', 'amount', 'tc')
    readonly_fields =  ('email', 'timestamp', 'transactionnumber', 'amount', 'tc')
    actions = [export_transaction_csv,]
    
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False
        
    def get_queryset(self, request):
        qs = super(TransactionAdmin, self).get_queryset(request)
        return qs.filter().exclude(signup='Y').exclude(completed='N')


def export_cdr_csv(model_admin, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=callrecords.csv'
    writer = csv.writer(response, csv.excel)
    response.write(u'\ufeff'.encode('utf8')) # BOM (optional...Excel needs it to open UTF-8 file properly)
    writer.writerow([
        smart_str(u"Account"),
        smart_str(u"DDI"),
        smart_str(u"Target"),
        smart_str(u"Time"),
        smart_str(u"Duration"),
        smart_str(u"Charged Minutes"),
        smart_str(u"Tariff"),
        smart_str(u"Charged Pence"),
    ])
    for obj in queryset:
#         charged_amount = obj.duration
#         if charged_amount is None:
#             pass
#         else:
#             charged_amount = int((charged_amount + 59) / 60) * 10

        writer.writerow([
            smart_str(obj.customer.email),
            smart_str(obj.msisdn),
            smart_str(obj.cld),
            smart_str(obj.connect),
            smart_str(obj.duration),
            smart_str(obj.charged_minutes),
            smart_str(obj.tariff),
            smart_str(obj.charge_pence),
        ])
    return response

export_cdr_csv.short_description = u"Export CSV"
        
@admin.register(Cdrs)
class CdrAdmin(admin.ModelAdmin):
    search_fields = ['msisdn', 'cld', 'connect']
    fields = ('customer_email', 'msisdn', 'cld', 'connect', 'duration', 'charged_amount')
    readonly_fields =  ('customer_email', 'msisdn', 'cld', 'connect', 'duration', 'charged_amount')
    actions = [export_cdr_csv,]
   
    def has_delete_permission(self, request, obj=None): # note the obj=None
        return False    

