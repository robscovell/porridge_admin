# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

class Areas(models.Model):
    country = models.IntegerField()
    area = models.IntegerField()
    name = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'areas'
        unique_together = (('country', 'area'),)


class Customers(models.Model):
    salutation = models.CharField(max_length=45, blank=True, null=True)
    firstname = models.CharField(max_length=45, blank=True, null=True)
    lastname = models.CharField(max_length=45, blank=True, null=True)
    address1 = models.CharField(max_length=45, blank=True, null=True)
    address2 = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    zip = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    balance = models.CharField(max_length=45, blank=True, null=True)
    blocked = models.IntegerField(blank=True, null=True)
    old_coms_login = models.CharField(unique=True, max_length=45, blank=True, null=True)
    password = models.CharField(max_length=45, blank=True, null=True) # Plain text passwords in the DB were a requirement of the Prison Service
    login = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return '%s, %s (%s)' % (self.lastname, self.firstname, self.login)

    class Meta:
        managed = False
        db_table = 'customers'
        verbose_name_plural = 'Customers'
        ordering = ('lastname',)
        


class Dids(models.Model):
    area = models.CharField(max_length=6, blank=True, null=True)
    number = models.CharField(max_length=11, blank=True, null=True)
    alternative_area = models.CharField(max_length=6, blank=True, null=True)
    target = models.CharField(max_length=20, blank=True, null=True)
    issued = models.IntegerField(blank=True, null=True)
    activated = models.IntegerField()
    issued_lock = models.IntegerField(blank=True, null=True)
#    customer = models.BigIntegerField(blank=True, null=True)
    customer = models.ForeignKey(Customers, db_column='customer')
    blocked = models.CharField(max_length=1, blank=True, null=True)
    old_coms_did = models.CharField(max_length=45, blank=True, null=True)
    old_coms_customer = models.CharField(max_length=45, blank=True, null=True)

    def __str__(self):
        return self.number

    class Meta:
        managed = False
        db_table = 'dids'
        verbose_name_plural = 'DDIs'
        verbose_name = 'DDI'


class Transactions(models.Model):
    customer = models.ForeignKey(Customers, db_column='customer')
    amount = models.IntegerField(blank=True, null=True)
    tc = models.CharField(max_length=45, blank=True, null=True)
    timestamp = models.DateTimeField(blank=True, null=True)
    completed = models.CharField(max_length=1, blank=True, null=True)
    signup = models.CharField(max_length=1, blank=True, null=True)
    upgauthcode = models.CharField(max_length=45, blank=True, null=True)
    transactionnumber = models.CharField(max_length=45, blank=True, null=True)

    def email(self):
        if self.customer:
            return self.customer.email
        else:
            return "Signup"

    def __str__(self):
        return "%s (%s, %s: %s)" % (self.transactionnumber, self.customer.lastname, self.customer.firstname, self.customer.email)
    
    class Meta:
        managed = False
        db_table = 'transactions'
        verbose_name_plural = 'Transactions'
        verbose_name = 'Transaction'


class Users(models.Model):
    username = models.CharField(max_length=45, blank=True, null=True)
    password = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class Cdrs(models.Model):
    customer = models.ForeignKey(Customers, db_column='customer')
    cli = models.CharField(db_column='CLI', max_length=45, blank=True, null=True)  # Field name made lowercase.
    cld = models.CharField(db_column='CLD', max_length=45, blank=True, null=True)  # Field name made lowercase.
    duration = models.IntegerField(blank=True, null=True)
    connect = models.DateTimeField(blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)
    msisdn = models.CharField(max_length=45, blank=True, null=True)
    charged_minutes = models.IntegerField(blank=True, null=True)
    tariff = models.IntegerField(blank=True, null=True)
    charge_pence = models.IntegerField(blank=True, null=True)
    
    def __str__(self):
        return "%s: %s --> %s (%s)" % (self.connect, self.msisdn, self.cld, self.customer.email)
        
    def customer_email(self):
        return self.customer.email
    
    def charged_amount(self):
        amount = self.duration
        if amount is None:
            return amount
        else:
            return int((amount + 59) / 60) * 10

    class Meta:
        managed = False
        db_table = 'CDRs'
        verbose_name_plural = 'Call Records'
        ordering = ('-connect',)



