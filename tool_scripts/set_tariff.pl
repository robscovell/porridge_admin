#!/usr/bin/perl

my $test_number = '00355213401234';

print &get_tariff($test_number);

sub get_tariff {
    my $number = shift;
    my $tariff = 10;
    open (IN, '<', '/home/robscovell/tool_scripts/rates.csv');
    
    while (<IN>) {
        chomp;
        my ($destination, $number_string, $rate) = split(/\,/);
        if ($number =~ /^00$number_string/) {
            $tariff = $rate * 100;
        }
    }
    close IN;
    return $tariff;
}
